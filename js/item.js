function createItem(title, status) {
  // Create HTML tags
  const item = document.createElement("li");
  const input = document.createElement("input");
  const span = document.createElement("span");

  // Set HTML properties
  input.setAttribute("type", "checkbox");
  input.checked = status;
  input.disabled = true;
  span.innerText = title;

  if (status) {
    span.classList.add("completed");
  }

  // Add HTML tags inside item
  item.appendChild(input);
  item.appendChild(span);

  return item;
}
