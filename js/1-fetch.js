console.log("1. Fetch exercise");

// Properties
const button = document.getElementById("button");
const list = document.getElementById("list");
const API_URL = "https://jsonplaceholder.typicode.com/todos/";

// Event listener
button.addEventListener("click", () => requestData(API_URL));

// Methods
function requestData(url) {
  console.log("2. Button clicked!");

  const response = fetch(url); // <= here im calling fetch ALONE!!!

  // HEY!!!
  // WAIT UNTIL THE FETCH IS COMPLETE
  // BEFORE !!!!
  // YOU RUN THIS NEXT LINES OF CODE
  console.log("3. ", response);
  console.log("4. ", response.json());
}

function displayItems() {}
