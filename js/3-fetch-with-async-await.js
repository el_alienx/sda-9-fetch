console.log("1. Fetch with Async & Await exercise");

// Properties
const button = document.getElementById("button");
const list = document.getElementById("list");
const API_URL = "https://jsonplaceholder.typicode.com/todos/";

// Event listener
button.addEventListener("click", () => requestData(API_URL));

// Methods
async function requestData(url) {
  console.log("2. Button clicked");

  const response = await fetch(url);
  const json = await response.json();

  console.log(json);
  displayItems(json);
}

function displayItems(json) {
  for (let i = 0; i < json.length; i++) {
    const item = createItem(json[i].title, json[i].completed);

    list.appendChild(item);
  }
}
