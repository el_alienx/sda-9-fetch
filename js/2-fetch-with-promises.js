console.log("1. Fetch with promises exercise");

// Properties
const button = document.getElementById("button");
const list = document.getElementById("list");
const API_URL = "https://jsonplaceholder.typicode.com/todos/";

// Event listener
button.addEventListener("click", () => requestData(API_URL));

// Methods
function requestData(url) {
  console.log("2. Button clicked");

  fetch(url)
    .then((response) => response.json())
    .then((json) => displayItems(json))
    .catch((error) => alert("As server error occurred.", error));
}

function displayItems(json) {
  for (let i = 0; i < json.length; i++) {
    const item = createItem(json[i].title, json[i].completed);

    list.appendChild(item);
  }
}
